<?php

/**
 * Implements hook_expirator_cron().
 */
function feeds_expirator_cron() {
  $limit = (int) variable_get('feeds_expirator_cron_limit', 50);

  $deleted_count = 0;
  $result = feeds_expirator_expired_feed_info_items_query($limit);
  foreach ($result as $feed_item) {
    if (feeds_expirator_delete($feed_item)) {
      ++$deleted_count;
    }
  }

  if ($deleted_count) {
    watchdog('feeds_expirator', '@count items expired.', array('@count' => $deleted_count), WATCHDOG_INFO);
  }
}

/**
 * Alter mapping targets for entities. Use this hook to add additional target
 * options to the mapping form of Node processors.
 *
 * If the key in $targets[] does not correspond to the actual key on the node
 * object ($node->key), real_target MUST be specified. See mappers/link.inc
 *
 * For an example implementation, see mappers/content.inc
 *
 * @param &$targets
 *   Array containing the targets to be offered to the user. Add to this array
 *   to expose additional options. Remove from this array to suppress options.
 *   Remove with caution.
 * @param $entity_type
 *   The entity type of the target, for instance a 'node' entity.
 * @param $bundle_name
 *   The bundle name for which to alter targets.
 */
function feeds_expirator_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  $targets['expire'] = array(
    'name' => t('Expiration date'),
    'description' => t('Feeds Expirator item expiration date.'),
    'callback' => '_feeds_expirator_set_expiration_callback',
  );
}

/**
 * Callback specified in hook_feeds_processor_targets_alter().
 *
 * @param $source
 *   Field mapper source settings.
 * @param $entity
 *   An entity object, for instance a node object.
 * @param $target
 *   A string identifying the target on the node.
 * @param $value
 *   The value to populate the target with.
 * @param $mapping
 *  Associative array of the mapping settings from the per mapping
 *  configuration form.
 */
function _feeds_expirator_set_expiration_callback($source, $entity, $target, $value, $mapping) {
  $entity->feeds_item->expire = feeds_to_unixtime($value, 0);
}

/**
 * Select feed item by its GUID, optionally limit search by importer ID.
 *
 * @param int|string $guid
 * @param string $importer_id
 * @return stdClass
 */
function feeds_expirator_feed_item_info_by_guid($guid, $importer_id = NULL) {
  $select = db_select('feeds_item')
               ->fields('feeds_item')
               ->condition('guid', $guid);

  if ($importer_id) {
    $select->condition('id', $importer_id);
  }

  return $select->execute()->fetchObject();
}

/**
 * Select feed item by its URL, optionally limit search by importer ID.
 *
 * @param int|string $guid
 * @param string $importer_id
 * @return stdClass
 */
function feeds_expirator_feed_item_info_by_url($url, $importer_id = NULL) {
  $select = db_select('feeds_item')
              ->fields('feeds_item')
              ->condition('url', $url);

  if ($importer_id) {
    $select->condition('id', $importer_id);
  }

  return $select->execute()->fetchObject();
}

/**
 * Query to fetch the expired feed items.
 *
 * @param int $limit
 * @return DatabaseStatementInterface|null
 */
function feeds_expirator_expired_feed_info_items_query($limit = 0) {
  $select = db_select('feeds_item')
            ->fields('feeds_item')
            ->condition('expire', array(1, REQUEST_TIME), 'BETWEEN')
            ->orderBy('expire', 'ASC');

  if ($limit > 0) {
    $select->range(0, $limit);
  }

  return $select->execute();
}

/**
 * Set expiration timestamp to given feed item.
 *
 * @param $feed_item
 * @param int $expire
 * @return bool|int
 */
function feeds_expirator_expire($feed_item, $expire = REQUEST_TIME) {
  $feed_item->expire = $expire;
  return drupal_write_record('feeds_item', $feed_item, array('entity_type', 'entity_id'));
}

/**
 * Deletes entity and related feed item.
 *
 * @param $feed_item
 * @return bool
 */
function feeds_expirator_delete($feed_item) {
  return entity_delete($feed_item->entity_type, $feed_item->entity_id);
}
